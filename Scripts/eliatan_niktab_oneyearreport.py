#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: Eliatan Niktab
"""
from statsmodels.sandbox.stats.multicomp import multipletests
import scipy.stats as st
import warnings
import pickle
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math 

# These imports are only required if you want to generate the
# genes_clustered_by_topology dictionary.
#from collections import defaultdict
#import community
#import leidenalg as la
#import igraph
#import networkx as nx


"""
This dictionary has a sample of anonymised patient data.
Dict key is the gene name, first item in the value list is the community ID for
that gene. 12 are NPC, 511 are controls.

Note that you have to put the file in your home directory. (File paths tested
only on Linux!)
"""
genes_clustered_by_toplogy = pickle.load(open("~/npc_mendelian_all.pickle", "rb"))


def map_community_to_genes(edge_list, algorithm = "la"):
    """
    Takes an edge list and algorithm name of either "lo" or "la".
    Returns a dict of:
        community -> gene name
    As specified by the algorithm.
    Only required if you want to generate the genes_clustered_by_topology dictionary.
    """
    if algorithm == "lo":
        # http://sociograph.blogspot.com/2012/11/visualizing-adjacency-matrices-in-python.html
        g_nx = nx.read_weighted_edgelist(edge_list)   
        louvain_community_dict = community.best_partition(g_nx)
        genes_clustered_by_toplogy_lo = defaultdict(set)
        for key in louvain_community_dict:
            genes_clustered_by_toplogy_lo[louvain_community_dict[key]].add(key)
        return genes_clustered_by_toplogy_lo
     
    else :    
        # algorithm is "la"
        # https://leidenalg.readthedocs.io/en/latest/index.html
        g_igraph = igraph.Graph.Read_Ncol(edge_list, directed=True)
        n_df = pd.read_csv(edge_list, sep=" ",  header=None)  
        part = la.find_partition(g_igraph, la.ModularityVertexPartition, n_iterations=-1, weights=n_df[2])
        genes_clustered_by_toplogy_la = defaultdict(set)
        for i in range(0, len(g_igraph.vs)): 
             genes_clustered_by_toplogy_la[part.membership[i]].add(g_igraph.vs[i]['name'])
        return genes_clustered_by_toplogy_la
        
        
def multipletests_correction(df):
    """
    Takes a data frame of N p-values as an N x 1 matrix.
    Add columns to the data from with the corresponding multiple test
    corrections.       
    """
    multitest_methods_names = {'b': 'Bonferroni',
                           #'s': 'Sidak',
                           'h': 'Holm',
                           #'hs': 'Holm-Sidak',
                           'sh': 'Simes-Hochberg',
                           'ho': 'Hommel',
                           'fdr_bh': 'FDR Benjamini-Hochberg',
                           'fdr_by': 'FDR Benjamini-Yekutieli',
                           'fdr_tsbh': 'FDR 2-stage Benjamini-Hochberg',
                           'fdr_tsbky': 'FDR 2-stage Benjamini-Krieger-Yekutieli',
                           'fdr_gbs': 'FDR adaptive Gavrilov-Benjamini-Sarkar'
                           }
    for corr in multitest_methods_names:
        df[multitest_methods_names[corr]] = multipletests(
                                                        df[0],
                                                        alpha=0.05,
                                                        method=corr,
                                                        is_sorted=False,
                                                        returnsorted=False)[1]
        
        
def best_fit_distribution(data, bins=200, ax=None):
    """
    Finds the best distribution to fit the log p value scores.
    This method was used to choose Johnson's distribution (one year 
    committee report).
    """
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0
    
    # Distribution to try to fit 
    DISTRIBUTIONS = [        
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]
    
    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf
    
    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:
        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')
                # fit dist to data
                params = distribution.fit(data)
                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]
                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))
                # if axis pass in add to plot
                try:
                    if ax:
                        pd.Series(pdf, x).plot(ax=ax)
                except Exception:
                    pass
                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)


def make_pdf(dist, params, size=10000):
    """
    Generate PDF of distribution
    """
    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]
    
    # Get sense of start and end points of distribution
    if arg:
        start = dist.ppf(0.01, *arg, loc=loc, scale=scale)
        end = dist.ppf(0.99, *arg, loc=loc, scale=scale)
    else:
        start = dist.ppf(0.01, loc=loc, scale=scale)
        end = dist.ppf(0.99, loc=loc, scale=scale)
    
    # Build PDF and turn into pandas Series
    x = np.linspace(start, end, size)
    y = dist.pdf(x, loc=loc, scale=scale, *arg)
    pdf = pd.Series(y, x)
    
    return pdf
   

"""
THIS SECTION PLOTS THE FISHER'S EXACT GRAPHS
"""

# CREATE DATFRAME FROM RESULTS DICTIONARY
df_gene = [key for key in genes_clustered_by_toplogy]
df_block = [genes_clustered_by_toplogy[key][0] for key in genes_clustered_by_toplogy]
df_betweenness = [genes_clustered_by_toplogy[key][1] for key in genes_clustered_by_toplogy]
df = pd.DataFrame({"gene" : df_gene,
                   "block" : df_block,
                   "betweenness": df_betweenness})
df = df.sort_values(by=["block", 'betweenness'], ascending=[False, True])
df = df.reset_index(drop=True)

gene_idx = {}
for index, row in df.iterrows():
    gene_idx[row[0]] = index

# FILTER OUT ALL EXCEPT NPC MODULE
col_stat = {}
for key in genes_clustered_by_toplogy:
    if  genes_clustered_by_toplogy[key][0] ==2:
        sick = sum(genes_clustered_by_toplogy[key][3][0:12])
        healthy = sum(genes_clustered_by_toplogy[key][3][12:])
        col_stat[gene_idx[key]]= stats.fisher_exact([[11, 0], [sick, healthy]])[1]
p_val = pd.DataFrame.from_dict(col_stat, orient='index')   

# DO MULTIPLE TEST CORRECTION
multipletests_correction(p_val)

# HACK: FIX GRAPH HEADER    
p_val = p_val.rename(columns = {0 :"Fisher's Exact not"})   
 
# CALCULATE -log10 OF P VALUES
# HACK: HANDLING LOG ZEROES
for col in p_val:
    temp_vol_val = []
    for item in p_val[col]:
        if item == 0:
            temp_vol_val.append(0)
        else:
            temp_vol_val.append(-(math.log10(item)))
    p_val[col] = temp_vol_val

# PLOT HISTOGRAM
fig = plt.figure(figsize=(10,12), dpi=400)
fig.subplots_adjust(hspace=0.4, wspace=0.4)
from scipy.stats import johnsonsb
from scipy.stats import lognorm
for col, i in zip(p_val, range(1, 11)):
    data = p_val[col]
    ax = fig.add_subplot(5, 2, i)
    binn = int(len(data)/100)
    y, x = np.histogram(data, bins=binn, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0
   
    params_gmamma= johnsonsb.fit(data[data > 5])
    pdf_gmamma = make_pdf(johnsonsb, params_gmamma)
    
    params_lognorm= lognorm.fit(data[data > 5])
    pdf_lognorm = make_pdf(lognorm, params_lognorm)
    
    data.plot(kind='hist', density = True, bins = binn, alpha = 0.2, color = "k")
    pdf_gmamma.plot(lw=2, label="Johnson's PDF", c = 'g', legend = True)
    pdf_lognorm.plot(lw=2, label=" Lognormal PDF", c = 'r', legend = True)
    plt.xlim(left=5)
    plt.ylim(top=0.03)
    plt.xlabel("{} Corrected p-value".format(col))
    ax.axvline(x=np.percentile(p_val[col], 99.9), color='k', linestyle='--', label='99th percentile')
    ax.plot(data, np.zeros(data.shape), 'b+', ms=20)
    ax.legend(loc='upper left')
plt.savefig("~/eniktab_oneyearcommittee_histogram.png",
            dpi=400,
            bbox_inches = 'tight',
            transparent = True)  
